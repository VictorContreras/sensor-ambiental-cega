#include <WiFi.h>
#include <Adafruit_MQTT.h>
#include <Adafruit_MQTT_Client.h>
#include <RTClib.h>
#include <Wire.h>
#include <Adafruit_Sensor.h>
#include "DHT.h"
#include "SD.h"
#include "SPI.h"
#include "FS.h"

#include "secrets.h"

#define DHTPIN 4     // Digital pin connected to the DHT sensor
#define DHTTYPE DHT22
DHT dht(DHTPIN, DHTTYPE);

#define PM_BAUDRATE 9600 // PM serial comm baudrate
#define PM_RX 17 // corresponde al RX y TX del sensor PM
#define PM_TX 16

unsigned int Pm25 = 0;
unsigned int Pm10 = 0;
//unsigned int Pm25c = 0;
//unsigned int Pm10c = 0;
float h;
float t;

unsigned int i = 0;
String datos;
String linealog;
//String linealog2;

// Intervalo de tiempo para realizar mediciones
unsigned long lastTime = 0;
unsigned long intervalo = 20000;

RTC_DS3231 RTC; // define the Real Time Clock object, connect only to default I2C port (21,22)

// Create an ESP8266 WiFiClient object to connect to the MQTT server.
WiFiClient client;
Adafruit_MQTT_Client mqtt(&client, AIO_SERVER, AIO_SERVERPORT, AIO_USERNAME, AIO_KEY);
Adafruit_MQTT_Publish t1 = Adafruit_MQTT_Publish(&mqtt, AIO_USERNAME "/feeds/CRA.t1");
Adafruit_MQTT_Publish t2 = Adafruit_MQTT_Publish(&mqtt, AIO_USERNAME "/feeds/CRA.t2");
Adafruit_MQTT_Publish t3 = Adafruit_MQTT_Publish(&mqtt, AIO_USERNAME "/feeds/CRA.t3");
Adafruit_MQTT_Publish t4 = Adafruit_MQTT_Publish(&mqtt, AIO_USERNAME "/feeds/CRA.t4");


Adafruit_MQTT_Publish *pubs[6] = {&t1, &t2, &t3, &t4};

// I2C 1st port pins
#define I2C_SDA              21
#define I2C_SCL              22

void setup() {
  Serial.begin(9600);

  /////// inicializacion SD /////////////
  if (!SD.begin(5)) {
    Serial.println("Card Mount Failed");
    return;
  }
  uint8_t cardType = SD.cardType();

  if (cardType == CARD_NONE) {
    Serial.println("No SD card attached");
    return;
  }
  /*
    Serial.print("SD Card Type: ");
    if(cardType == CARD_MMC){
      Serial.println("MMC");
    } else if(cardType == CARD_SD){
      Serial.println("SDSC");
    } else if(cardType == CARD_SDHC){
      Serial.println("SDHC");
    } else {
      Serial.println("UNKNOWN");
    }

    listDir(SD, "/", 2);
    writeFile(SD, "/hello.txt", "Hello ");
    appendFile(SD, "/hello.txt", "World!\n");
  */
  ////// Start I2C communication para reloj ////////////
  Wire.begin(I2C_SDA, I2C_SCL, 400000);

  if (!RTC.begin()) {
    Serial.println("RTC failed");
  }
  /*
    DateTime now = RTC.now();

    ///// Definimos nombre archivo guardado en SD MMYYYY.csv Creamos un archivo por mes ///////////
    String datos = "/" + String(now.month(), DEC) + String(now.year(), DEC) + ".csv" ;


    // If the data.txt file doesn't exist
    // Create a file on the SD card and write the data labels
    File file = SD.open(datos);
    if (!file) {
      Serial.println("File doens't exist");
      Serial.println("Creating file...");
      writeFile(SD, datos.c_str(), "Fecha, Humedad [RH%], Temperatura [ºC], PM2.5 [mug/cm^3], PM10 [mug/cm^3], PM2.5c [mug/cm^3], PM10c [mug/cm^3] \r\n");
    }
    else {
      Serial.println("File already exists");
    }
    file.close();
  */
  delay(2000);

  ////////// Inicializamos sensores de humedad/temperatura y MP /////////////////

  dht.begin();
  Pm25 = 0;
  Pm10 = 0;


  ////// Nos comunicamos con SDS011 /////////////
  Serial.println("SDS011 software serial port");
  Serial2.begin(PM_BAUDRATE, SERIAL_8N1, PM_RX, PM_TX); //Open software serial port at 9600bps
}

void loop() {

  if ((millis() - lastTime) > intervalo) {


    DateTime now = RTC.now();

    Serial.print('"');
    Serial.print(now.year(), DEC);
    Serial.print("/");
    Serial.print(now.month(), DEC);
    Serial.print("/");
    Serial.print(now.day(), DEC);
    Serial.print(" ");
    Serial.print(now.hour(), DEC);
    Serial.print(":");
    Serial.print(now.minute(), DEC);
    Serial.print(":");
    Serial.print(now.second(), DEC);
    Serial.println('"');

    // Reading temperature or humidity takes about 250 milliseconds!
    // Sensor readings may also be up to 2 seconds 'old' (its a very slow sensor)
    h = dht.readHumidity();
    // Read temperature as Celsius (the default)
    t = dht.readTemperature();

    // Check if any reads failed and exit early (to try again).
    if (isnan(h) || isnan(t)) {
      Serial.println(F("Failed to read from DHT sensor!"));
      return;
    }

    ProcessSerialData();
    // Normalization function written by Zbyszek Kiliański, Piotr Paul
    // https://github.com/piotrkpaul/esp8266-sds011

    //  Pm25c = Pm25 / (1.0 + 0.48756 * pow((h / 100.0), 8.60068));
    //  Pm10c = Pm10 / (1.0 + 0.81559 * pow((h / 100.0), 5.83411));

    Serial.print(F("Humidity: "));
    Serial.print(h);
    Serial.print(F("%  Temperature: "));
    Serial.print(t);
    Serial.print(F("°C "));
    Serial.print(", ");
    Serial.print(float(Pm25) / 10.0);
    Serial.print(", ");
    Serial.println(float(Pm10) / 10.0);
    /*
    Serial.print(", ");
    Serial.print(float(Pm25c) / 10.0);
    Serial.print(", ");
    Serial.println(float(Pm10c) / 10.0);
    */

    logSDCard();


    if (WiFi_connect() == true && MQTT_connect() == true) {
      delay(2000);

      //WiFi_connect();
      //MQTT_connect();

      t1.publish(h);
      delay(500);
      t2.publish(t);
      delay(500);
      t3.publish(Pm25 / 10.0);
      delay(500);
      t4.publish(Pm10 / 10.0);
      delay(500);
      /*      t5.publish(Pm25c / 10.0);
            delay(500);
            t6.publish(Pm10c / 10.0);*/
    }

    lastTime = millis();
  }
}

bool WiFi_connect() {
  uint8_t retries = 5;
  //Serial.println(WiFi.macAddress());
  Serial.println("Connecting to WiFi...");
  WiFi.begin(WLAN_SSID, WLAN_PASS);

  while (WiFi.status() != WL_CONNECTED) {
    delay(5000);
    Serial.print(".");
    retries--;
    if (retries == 0) {

      return false;
    }
  }
  //Serial.println();

  Serial.println("..WiFi connected");
  return true;
  //Serial.println("IP address: "); Serial.println(WiFi.localIP());

}

bool MQTT_connect()
{
  int8_t ret;
  //int8_t ret=mqtt.connect();

  // Stop if already connected.
  if (mqtt.connected()) {
    return true;
  }

  Serial.print("Connecting to MQTT... ");

  uint8_t retries = 5;

  while ((ret = mqtt.connect()) != 0) { // connect will return 0 for connected
    Serial.println(mqtt.connectErrorString(ret));
    Serial.println("Retrying MQTT connection in 5 seconds...");
    Serial.print(retries);
    Serial.print("... ");
    mqtt.disconnect();

    delay(5000); // wait 5 seconds

    retries--;
    if (retries == 0) {
      // basically die and wait for WDT to reset me
      //while (1);
      // ret=0;
      // Serial.println("MQTT failed");
      Serial.println(mqtt.connectErrorString(ret));
      return false;
    }
  }

  Serial.println("MQTT Connected!");
  return true;
}


void ProcessSerialData()
{
  uint8_t mData = 0;
  uint8_t i = 0;
  uint8_t mPkt[10] = {0};
  uint8_t mCheck = 0;
  while (Serial2.available() > 0)
  {
    // from www.inovafitness.com
    // packet format: AA C0 PM25_Low PM25_High PM10_Low PM10_High 0 0 CRC AB
    mData = Serial2.read();
    delay(2);//wait until packet is received
    //Serial.println(mData);
    //Serial.println("*");
    if (mData == 0xAA) //head1 ok
    {
      mPkt[0] =  mData;
      mData = Serial2.read();
      if (mData == 0xc0) //head2 ok
      {
        mPkt[1] =  mData;
        mCheck = 0;
        for (i = 0; i < 6; i++) //data recv and crc calc
        {
          mPkt[i + 2] = Serial2.read();
          delay(2);
          mCheck += mPkt[i + 2];
        }
        mPkt[8] = Serial2.read();
        delay(1);
        mPkt[9] = Serial2.read();
        if (mCheck == mPkt[8]) //crc ok
        {
          Serial2.flush();
          Serial2.write(mPkt, 10);

          Pm25 = (uint16_t)mPkt[2] | (uint16_t)(mPkt[3] << 8);
          Pm10 = (uint16_t)mPkt[4] | (uint16_t)(mPkt[5] << 8);
          if (Pm25 > 9999)
            Pm25 = 9999;
          if (Pm10 > 9999)
            Pm10 = 9999;
          //Serial.println(Pm25);
          //Serial.println(Pm10);
          //get one good packet
          return;
        }
      }
    }
  }
}

void listDir(fs::FS &fs, const char * dirname, uint8_t levels) {
  Serial.printf("Listing directory: %s\n", dirname);

  File root = fs.open(dirname);
  if (!root) {
    Serial.println("Failed to open directory");
    return;
  }
  if (!root.isDirectory()) {
    Serial.println("Not a directory");
    return;
  }

  File file = root.openNextFile();
  while (file) {
    if (file.isDirectory()) {
      Serial.print("  DIR : ");
      Serial.println(file.name());
      if (levels) {
        listDir(fs, file.name(), levels - 1);
      }
    } else {
      Serial.print("  FILE: ");
      Serial.print(file.name());
      Serial.print("  SIZE: ");
      Serial.println(file.size());
    }
    file = root.openNextFile();
  }
}

void writeFile(fs::FS &fs, const char * path, const char * message) {
  Serial.printf("Writing file: %s\n", path);

  File file = fs.open(path, FILE_WRITE);
  if (!file) {
    Serial.println("Failed to open file for writing");
    return;
  }
  if (file.print(message)) {
    Serial.println("File written");
  } else {
    Serial.println("Write failed");
  }
  file.close();
}

void appendFile(fs::FS &fs, const char * path, const char * message) {
  Serial.printf("Appending to file: %s\n", path);

  File file = fs.open(path, FILE_APPEND);
  if (!file) {
    Serial.println("Failed to open file for appending");
    return;
  }
  if (file.print(message)) {
    Serial.println("Message appended");
  } else {
    Serial.println("Append failed");
  }
  file.close();
}

// Write the sensor readings on the SD card
void logSDCard() {

  DateTime now = RTC.now();

  ///// Definimos nombre archivo guardado en SD MMYYYY.csv Creamos un archivo por mes ///////////
  String datos = "/" + String(now.month(), DEC) + String(now.year(), DEC) + ".csv" ;


  // If the data.txt file doesn't exist
  // Create a file on the SD card and write the data labels
  File file = SD.open(datos);
  if (!file) {
    Serial.println("File doens't exist");
    Serial.println("Creating file...");
    writeFile(SD, datos.c_str(), "Fecha, Humedad [RH%], Temperatura [ºC], PM2.5 [mug/cm^3], PM10 [mug/cm^3] \r\n");
    //writeFile(SD, datos.c_str(), "Fecha, Humedad [RH%], Temperatura [ºC], PM2.5 [mug/cm^3], PM10 [mug/cm^3], PM2.5c [mug/cm^3], PM10c [mug/cm^3] \r\n");
  }
  else {
    Serial.println("File already exists");
  }
  file.close();

  linealog  = String(now.day(), DEC) + "/" + String(now.month(), DEC) + "/" + String(now.year(), DEC) + " " + String(now.hour(), DEC) + ":" + String(now.minute(), DEC) + ":" + String(now.second(), DEC) +
              ", " + String(h) + ", " + String(t) + ", " + String(Pm25 / 10.0) + ", " + String(Pm10 / 10.0) + "\r\n";
 /* linealog  = String(now.day(), DEC) + "/" + String(now.month(), DEC) + "/" + String(now.year(), DEC) + " " + String(now.hour(), DEC) + ":" + String(now.minute(), DEC) + ":" + String(now.second(), DEC) +
              ", " + String(h) + ", " + String(t) + ", " + String(Pm25 / 10.0) + ", " + String(Pm10 / 10.0) + ", " + String(Pm25c / 10.0) + ", " + String(Pm10c / 10.0) + "\r\n";
              */

  Serial.print("Save data: ");
  Serial.print(linealog);
  //Serial.println(linealog2);
  appendFile(SD, datos.c_str(), linealog.c_str());
  //appendFile(SD, datos.c_str(), linealog2.c_str());
}
